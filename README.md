# OpenSSL Engine pro podporu CryptoAPI: Next Generation
Tento textový soubor popisuje obsah přílohy bakalářské práce.
Dále poskytuje návod pro přípravu prostředí, ve kterém může být otestována
funkcionalita dodaného kryptografického modulu.

## Struktura projektu
```
ecng
+-- docs/
+-- src/
    +-- makefile
    +-- engine/
        +-- e_capi.c
        +-- e_cng.c
    +-- test/
        +-- client.c
        +-- server.c
        +-- eng_loader.c
        +-- conf/
            +--- example.conf
    +-- certs/
        +--- generate_ecdsa_certs.cmd
        +--- generate_rsa_certs.cmd
```

## Kompilace OpenSSL

Nejdříve je potřeba získat projekt OpenSSL do nějaké složky %OPENSSL_DIR%.  
`git clone https://github.com/openssl/openssl.git %OPENSSL_DIR%`  
Protože master větev neodkazuje na LTS verzi, je vhodné provést
checkout na větev 'OpenSSL_1_1_1-stable'.
Pro úspěšné sestavení projektu je nutné mít k dispozici sadu nástrojů,
které jsou vypsané v souboru %OPENSSL_DIR%\NOTES.WIN, tedy:  
- Perl. Buď ActiveState Perl (https://www.activestate.com/ActivePerl), 
            nebo Strawberry Perl (http://strawberryperl.com). A Perl modul
            Text::Template dostupný z CPAN.
- Microsoft Visual C compiler.
- Netwide Assembler (https://www.nasm.us)

V dalším kroku se provede konfigurace kompilace (více v %OPENSSL_DIR%\INSTALL),
pro vývoj byla použito následující nastavení.  
`perl Configure VC-WIN32 --prefix=%OPENSSL_DIR% --openssldir=%OPENSSL_DIR%`

Pak už jen stačí spustit příkaz `nmake`, což zkompiluje všechny části
OpenSSL projektu do adresáře %OPENSSL_DIR%

## Kompilace testovacího prostředí

Obsah adresáře src\impl\ je vhodné vykopírovat do lokálního úložiště
do nějakého adresáře %CNG_DIR%.
Nejdříve je potřeba uvnitř %CNG_DIR%\makefile upravit první řádek tak,
aby proměnná OPENSSL_DIR obsahovala skutečnou cestu k projektu OpenSSL,
tedy %OPENSSL_DIR%. Tím by měla být zaručena funkčnost celého makefile.

`nmake generate` spustí cmd skripty uložené v adresáři certs\,
                 nejdříve se pomocí aplikace openssl vygenerují
                 certifikáty a klíče pro ECDSA a RSA algoritmy do
                 složky certs\. Vygenerované páry spolu utvoří
                 pfx formát a následně budou uloženy do úložiště
                 certifikátů powershell příkazem.

`nmake engines` vytvoří sdílené knihovny kryptografických modulů
                capi a cng do adresáře engine\. 

`nmake tests` zkompiluje aplikaci server, klient a eng_loader do
              adresáře test\.

`nmake copy_libs` překopíruje knihovny libcrypto.dll a libssl.dll
                  do aktuálního adresáře. Jsou potřeba ke spuštení
                  aplikací.

`nmake all` spustí všechny výše uvedené úkony.

## Spouštění

Aplikace test\eng_loader.exe očekává jako první argument cestu k dll modulu.  
např.: `.\test\eng_loader.exe .\engine\capi.dll`

Aplikace server.exe vystartuje virtuální server na na adrese 127.0.0.1:27015.
Po spuštění client.exe by mělo dojít k úspěšnému spojení, což potvrdí klient
tak, že zobrazí zprávu "To bude dobrý.". Bližší podrobnosti o průběhu spojení
předávají obě aplikace na standardní výstup.  
  např.: `.\test\server.exe` nebo `.\test\client.exe`

Aplikace server.exe a client.exe jsou nastavitelné prostřednictvím
konfiguračního souboru. Ukázkový konfigurační soubor je dostupný
v souboru .\example.conf. Obsahuje taktéž stručný popis
ke každé položce. Pro zohlednění konfigurace v souboru je nutné
předat aplikaci cestu k tomuto souboru jako první argument.
Při spuštění bez doplňující konfigurace bude využito výchozí nastavení.  
  např.: `.\test\server.exe .\example.conf` nebo
         `.\test\client.exe .\example.conf`

Pro ještě snazší start serveru a klienta byl vytvořen script
`_run.cmd`, který spustí aplikaci server a klient v nových oknech.
Lze mu předat cestu ke konfiguračnímu souboru, který bude aplikován
na obě aplikace, například `_run.cmd example.conf`.


| klíč               | popis                                               | výchozí hodnota                   |
|--------------------|-----------------------------------------------------|-----------------------------------|
| MAX_TLS_VERSION    | nejvyšší verze TLS protokolu                        | 1.2                               |
| RSA_CA_PATH        | cesta k CA vystavené pomocí RSA klíče               | "certs\\ca_rsa.pem"               |
| ECDSA_CA_PATH      | cesta k CA vystavené pomocí ECDSA klíče             | "certs\\ca_ecdsa.pem"             |
| SERVER_ENGINE_PATH | cesta k dll modulu pro použití serverem             | "engine\\cng.dll"                 |
| CLIENT_ENGINE_PATH | cesta k dll modulu pro použití klientem             | "engine\\cng.dll"                 |
| SERVER_RSA_CERT    | jméno RSA certifikátu serveru v úložišti            | "localhostServerRSA"              |
| SERVER_ECDSA_CERT  | jméno ECDSA certifikátu serveru v úložišti          | "localhostServerECDSA"            |
| ECDSA_ONLY         | k navázaní spojení bude využit jen ECDSA algoritmus |                                   |
| RSA_ONLY           | k navázaní spojení bude využit jen RSA algoritmus   |                                   |
| NO_RSA_PSS         | RSA-PSS bude zakázán                                |                                   |
| CERT_REQUEST       | server odešle CertificateRequest                    |                                   |

Poslední čtyři konfigurační parametry nejsou ve výchozím nastavení použity.
Pokud budou v konfiguračním souboru specifikovány, nezáleží na jejich hodnotě.  
