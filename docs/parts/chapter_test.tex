\chapter{Testování}

Poslední kapitola se věnuje přípravě testovacího prostředí.
Nejdříve bude připravena aplikace client, která k síťové
komunikaci používá komponentu \verb|BIO|. Kryptografický
modul využívá pouze v případě, že součástí TLS Handshake
je zpráva Certificate Request. Server je připraven
jiným způsobem a kryptografický modul potřebuje pro
svoji funkčnost vždy. Protože jsou tyto dvě aplikace
určeny k uzavření bezpečného síťového spojení, potřebují
mít k dispozici sadu digitálních certifikátů, proto tato
kapitola představuje, jak vygenerovat testovací certifikáty.
Poslední aplikace je izolovaná od předchozích a pouze prezentuje
nadstandardní příkazy. Závěr kapitoly se věnuje diskuzi
o výsledku testování.

\section{Příprava klienta}

Pokud je ambicí aplikace pracovat s knihovnou libssl, je nutné ji nejdříve inicializovat.
Starší verze OpenSSL vyžadovaly zvláštní volání pro načtení chybových hlášek, současná
verze celý inicializační proces zabaluje do volání \verb|OPENSSL_init_ssl()|.
Tato funkce do paměti načte kryptografické EVP struktury pro šifrování a hešování.
Pokud si je tedy vývojář aplikace jistý, které algoritmy se budou za běhu využívat,
může ušetřit paměť a načíst jen potřebné struktury.

V dalším kroku je potřeba vytvořit a nastavit \verb|SSL_CTX|, který udržuje
parametry pro budoucí spojení. Při vytváření nové kontextové struktury je nutné
specifikovat verzi TLS protokolu. Dokumentace \cite{osslSslCtx} doporučuje vytvořit strukturu
podporující všechny verze TLS a v případě potřeby verzi zdola nebo shora
omezit při konfiguraci existující reference. V kontextu lze dále
specifikovat jaké konkrétní šifrové sady mohou být použity k navázání spojení.
Tím je možné vynutit, aby se v první části TLS Handshake využil pouze RSA
algoritmus, nebo ECDSA algoritmus. Bez bližšího určení klient nabídne serveru
prioritní seznam šifrových sad, které jsou knihovnou libssl pokládány za bezpečné.
Podobně lze specifikovat i podpisové algoritmy pro TLS verze alespoň 1.2.
Dalším důležitým krokem při konfiguraci je určit certifikáty 
autority, které rozhodnou o důvěryhodnosti certifikátu serveru.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
SSL_CTX* ctx = SSL_CTX_new(TLS_method());
SSL_CTX_set_max_proto_version(ctx, TLS1_2_VERSION);
SSL_CTX_set_cipher_list(ctx, "DHE-RSA-AES256-SHA");
SSL_CTX_set1_sigalgs_list(ctx, "RSA+SHA256");
SSL_CTX_load_verify_locations(ctx, "/path/to/ca.pem", NULL);
\end{minted}
\caption{Vytvoření a konfigurace struktury SSL\_CTX na straně klienta}
\label{code:clientCTX}
\end{listing}

\else

\begin{verbatim}
SSL_CTX* ctx = SSL_CTX_new(TLS_method());
SSL_CTX_set_max_proto_version(ctx, TLS1_2_VERSION);
SSL_CTX_set_cipher_list(ctx, "DHE-RSA-AES256-SHA");
SSL_CTX_set1_sigalgs_list(ctx, "RSA+SHA256");
SSL_CTX_load_verify_locations(ctx, "/path/to/ca.pem", NULL);
\end{verbatim}

\fi

\noindent Výpis kódu \ifdefined\ovrlf \ref{code:clientCTX} \fi ukazuje vytvoření nové
struktury \verb|SSL_CTX| s omezenou konfigurací. Nejdříve je nastaveno, že nejvyšší
možnou verzí TLS protokolu je 1.2. Následně je vynucená jedna konkrétní šifrová sada
a jeden podpisový algoritmus, který znemožní využití RSA-PSS podpisového schématu.
Na závěr je určen certifikát certifikační autority.

Pokud existuje více autorit, proti kterým se má certifikát serveru validovat,
není nutné volat funkcí z příkladu pro každý soubor zvlášť. Prvním ulehčením
je předat funkci soubor, ve kterém jsou certifikáty zřetězené. Druhým řešením
je místo souboru specifikovat celý adresář, ze kterého se mají certifikáty načíst.
V tom případě je ale nutné, aby tyto soubory byly pojmenovány jako hash jména
autority.

Posledním krokem konfigurace kontextu je připravit klienta na Certificate
Request, tedy žádost serveru o autenizaci klienta v průběhu TLS Handshake.
Toho lze docílit více způsoby. Prvním je předem určit soukromý klíč a certifikát, čímž dojde k jejich načtení bez závislosti na tom, zda budou skutečně potřeba.
Druhou a tímto klientem využívanou možností, je specifikovat pouze
callback funkci, která se zavolá po
přijetí žádosti o předložení certifikátu klienta. Do takové funkce vstupuje
struktura \verb|SSL| reprezentující probíhající spojení, ze které lze
vyčíst serverem akceptovatelná jména certifikačních autorit. Na výstupu
se očekává certifikát vydaný jednou z těchto autorit v reprezentaci
strukturou \verb|X509| a příslušný soukromý klíč v \verb|EVP_PKEY|.
Obě tyto položky jsou automaticky přiřazeny aktuálnímu spojení.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
STACK_OF(X509_NAME) *ca_list = SSL_get0_peer_CA_list(ssl);
ENGINE *engine = load_engine("/path/to/engine.dll");
ENGINE_load_ssl_client_cert(engine, ssl, ca_list,
                            &x509, &pkey, NULL,
                            NULL, NULL);
\end{minted}
\caption{Callback funkce pro vyřízení Certificate Requestu}
\label{code:certReqCallback}
\end{listing}

\else

\begin{verbatim}
STACK_OF(X509_NAME) *ca_list = SSL_get0_peer_CA_list(ssl);
ENGINE *engine = load_engine("/path/to/engine.dll");
ENGINE_load_ssl_client_cert(engine, ssl, ca_list,
                            &x509, &pkey, NULL,
                            NULL, NULL);
\end{verbatim}

\fi

Výpis kódu \ifdefined\ovrlf \ref{code:certReqCallback} \fi zobrazuje část implementace
callback funkce pro výběr certifikátu a klíče, nalezení vhodného
páru je delegováno na engine. Nejdříve je získán
seznam akceptovatelných certifikačních autorit. Funkce
\verb|load_engine()| na základě specifikované cesty
ke sdílené knihovně vytvoří funkcionální referenci modulu
a označí jeho implementace za defaultní. Poslední příkaz
využívá Engine API pro získání vhodného certifikátu a soukromého klíče.
Poslední tři argumenty nevyužívá ani capi modul, ani cng.
Nakonec je celá callback funkce přiřazena ssl kontextu
prostřednictvím \verb|SSL_CTX_set_client_cert_cb()|.

Až poté, co je správně inicializován \verb|SSL_CTX|, může nastat spojení se serverem.
Pro implementaci klienta jsou použity vysokoúrovňová volání modulu \verb|BIO|,
který kromě standardních I/O operací umí zprostředkovat šifrovanou komunikaci
po síti. Nové \verb|BIO| struktuře stačí specifikovat SSL kontext a adresu serveru,
následně může proběhnout samotné spojení.
Po provedení čtyř kroků z výpisu \ifdefined\ovrlf \ref{code:clientConn} \fi
lze zasílat a číst zprávy pomocí \verb|BIO_read()| a \verb|BIO_puts()|.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
BIO *web = BIO_new_ssl_connect(ctx);
BIO_set_conn_hostname(web, "127.0.0.1:27015");
BIO_do_connect(web);
BIO_do_handshake(web);
\end{minted}
\caption{Zahájení spojení pomocí modulu BIO}
\label{code:clientConn}
\end{listing}

\else

\begin{verbatim}
BIO *web = BIO_new_ssl_connect(ctx);
BIO_set_conn_hostname(web, "127.0.0.1:27015");
BIO_do_connect(web);
BIO_do_handshake(web);
\end{verbatim}

\fi

Pokud aplikace chce získat podrobnosti o spojení, je nutné si z \verb|BIO| 
vyžádat \verb|SSL| strukturu. Ta mimo jiné nese informace o serverovém certifikátu,
o dohodnuté šifrové sadě a o dohodnuté verzi TLS protokolu.


\section{Příprava serveru}

Pro přípravu serveru bylo zvoleno jiné řešení než prostřednictvím modulu \verb|BIO|.
Protože server taktéž využívá knihovnu libssl, začíná zdrojový kód inicializací knihovny.
V dalším kroku se pro server připraví socket pomocí rozhraní Winsock.

Tato aplikace také prezentuje možné využití kryptografického modulu, proto
následuje jeho načtení a konfigurace. První možností jak získat strukturální referenci
je voláním \verb|ENGINE_by_id()|, jež deleguje načtení na dynamic engine. V tom případě
se vyhledávání modulu omezí na adresář definovaný v proměnné \verb|ENGINESDIR|,
což je typicky v podadresáři OpenSSL projektu \verb|.\lib\engines-1_1\|. Pro
větší flexibilitu je výhodnější komunikovat s dynamic modulem přímo. Následně je nutné
přenastavit \verb|ENGINE_TABLE| tak, aby se využívaly implementace algoritmů
poskytované právě načteným modulem.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
ENGINE_load_dynamic();
ENGINE *e = ENGINE_by_id("dynamic");
ENGINE_ctrl_cmd_string(e, "SO_PATH", "/path/to/dll", 0);
ENGINE_ctrl_cmd_string(e, "LOAD", NULL, 0);
ENGINE_set_default(e, ENGINE_METHOD_ALL);
\end{minted}
\caption{Načtení kryptografického modulu}
\label{code:serverEngine}
\end{listing}

\else

\begin{verbatim}
ENGINE_load_dynamic();
ENGINE *engine = ENGINE_by_id("dynamic");
ENGINE_ctrl_cmd_string(engine, "SO_PATH", "/path/to/dll", 0);
ENGINE_ctrl_cmd_string(engine, "LOAD", NULL, 0);
ENGINE_set_default(engine, ENGINE_METHOD_ALL);
\end{verbatim}

\fi

\newpage
Výpis kódu \ifdefined\ovrlf \ref{code:serverEngine} \fi ukazuje dynamické
načítání externího modulu. Nejdříve je nutné získat dynamic engine do interního
spojového seznamu. Druhý řádek výpisu získá strukturální referenci modulu,
který zprostředkuje načtení externího kryptografického modulu. Dynamic modul se řídí
pomocí nadstandardních příkazů. \verb|SO_PATH| specifikuje cestu
ke sdílené knihovně, která obsahuje požadovaný engine a příkazem \verb|LOAD|
dojde k načtení. Do původní reference se dostane strukturální reference
hledaného modulu. Poslední řádek výpisu zaručí, že implementace všech algoritmů,
které modul poskytuje, budou považovány za výchozí.

Pro server se také musí vytvořit \verb|SSL_CTX|, nositele
konfigurace pro příchozí spojení. Tentokrát je potřeba kontextové struktuře
přidělit certifikát jakým se bude server prokazovat a klíč pro asymetrickou
kryptografii. Server pro uchování certifikátu využívá úložiště operačního
systému Windows, proto implementuje funkci \verb|get_cert_from_winstore()|,
která vyhledá v systémovém úložišti \verb|MY| certifikát podle jména subjektu.
Jedná se pouze o zjednodušenou funkci \verb|find_cert()| známé z capi modulu,
jen na závěr je certifikát převeden do struktury \verb|X509|, aby byl
čitelný OpenSSL knihovnou.


\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
SSL_CTX *ctx = SSL_CTX_new(TLS_method());
ENGINE_init(e);

X509* cert = get_cert_from_winstore("localhost");
SSL_CTX_use_certificate(ctx, cert);

EVP_PKEY *key = ENGINE_load_private_key(e, "localhost",
                                        NULL, NULL);
SSL_CTX_use_PrivateKey(ctx, key);
\end{minted}
\caption{Vytvoření a konfigurace SSL\_CTX na straně serveru}
\label{code:serverCTX}
\end{listing}

\else

\begin{verbatim}
SSL_CTX *ctx = SSL_CTX_new(TLS_method());
ENGINE_init(e);

X509* cert = get_cert_from_winstore("localhost");
SSL_CTX_use_certificate(ctx, cert);

EVP_PKEY *key = ENGINE_load_private_key(e, "localhost",
                                        NULL, NULL);
SSL_CTX_use_PrivateKey(ctx, key);
\end{verbatim}

\fi

\noindent Výpis kódu \ifdefined\ovrlf \ref{code:serverCTX} \fi
zobrazuje průběh přípravy SSL kontextové struktury. Po vytvoření nové \verb|SSL_CTX|
se získá funkcionální reference modulu. Následně je v úložišti vyhledán certifikát,
jehož jméno subjektu obsahuje řetězec localhost a v \verb|X509| reprezentaci
je předán kontextové struktuře. Pro získání klíče k příslušnému certifikátu
se již využívá řešení vytvořené modulem. Připravená \verb|EVP_PKEY| struktura
je předána kontextu. Do kontextové struktury lze připojit více
certifikátů a klíčů. Jaký pár se k zahájení spojení nakonec využije záleží na dohodnuté
šifrové sadě.

Pokud má server po klientovi vyžadovat autorizaci, je nutné konfiguraci ještě obohatit.
Nejdříve se musí, podobně jako v případě klienta, specifikovat certifikáty certifikačních
autorit a následně vytvořit jejich jmenný seznam. 

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
SSL_CTX_load_verify_locations(ctx, "ca.pem", NULL);
STACK_OF(X509_NAME) *certs = SSL_load_client_CA_file("ca.pem");
SSL_CTX_set_client_CA_list(ctx, certs);
SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER |
                        SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
\end{minted}
\caption{Vynucení autorizace klienta}
\label{code:serverCertReq}
\end{listing}

\else

\begin{verbatim}
SSL_CTX_load_verify_locations(ctx, "ca.pem", NULL);
STACK_OF(X509_NAME) *certs = SSL_load_client_CA_file("ca.pem");
SSL_CTX_set_client_CA_list(ctx, certs);
SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER |
                        SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
\end{verbatim}

\fi

\noindent Výpis \ifdefined\ovrlf \ref{code:serverCertReq} \fi zobrazuje
konfiguraci serverového SSL kontextu tak, aby byla vždy zaslána zpráva
Certificate Request. Nejdříve dojde k určení certifikátu autority,
který bude validovat klienta. Tento příkaz nijak neovlivní obsah
zprávy Certificate Request, proto je nutné vzápětí certifikát znovu načíst
a zařadit ho do jmenného seznamu, čímž se už stane součástí zprávy.
Nakonec je nařízeno, že pokud ověření certifikátu klienta selže,
nepodaří se navázat spojení.

Po inicializaci struktury může server přistoupit k utváření spojení s klienty.
Nejdříve server vyčkává, dokud nepřijde žádost ke spojení, poté vytvoří novou
\verb|SSL| strukturu, která spojení reprezentuje. Po přesměrování komunikačního
kanálu na příslušný socket vyčkává dokud, klient nezahájí handshake.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
int socket = accept(); /*funkce z WinSock2.h*/
ssl = SSL_new(ctx);
SSL_set_fd(ssl, client);
SSL_accept(ssl);
SSL_write(ssl, "Hello there", 11);
SSL_shutdown(ssl);
\end{minted}
\caption{Vytvoření spojení s klientem}
\label{code:serverConn}
\end{listing}

\else

\begin{verbatim}
int socket = accept(); /*funkce z WinSock2.h*/
ssl = SSL_new(ctx);
SSL_set_fd(ssl, socket);
SSL_accept(ssl);
SSL_write(ssl, "Hello there", 11);
SSL_shutdown(ssl);
\end{verbatim}

\fi

\noindent Výpis kódu \ifdefined\ovrlf \ref{code:serverConn} \fi je průvodcem
připojením nového klienta k serveru. První řádek pracuje s rozhraním WinSock,
povolí klientovi přístup a vrátí číslo socketu, na který server následně přesměruje
I/O operace. Na čtvrtém řádku server reaguje na žádost o provedení TLS Handshake a 
po jeho úspěšném dokončení je klientovi poslána krátká zpráva. Aniž by server čekal
na jakoukoliv reakci klienta, ukončuje navázané spojení.

\newpage
\section{Příprava certifikátů} \label{sec:certs}

Každý server na internetu, který umožňuje komunikaci pomocí protokolu HTTPS,
vystupuje pod svým certifikátem. Digitální certifikát je množina dat, která
identifikuje daný subjekt. Vytvoření takového certifikátu není náročné a
lze mu přiřadit lživá data, sám o sobě tedy důvěryhodnost nepřináší.
Aby byla potvrzena jeho pravost, musí se za něj zaručit certifikační
autorita.

Certifikační autorita disponuje soukromým klíčem a certifikátem, jehož obsahem
je standardně veřejný klíč a informace o subjektu. Pokud nový subjekt usiluje
o získání ověřeného certifikátu, předá autoritě CSR s informaci na jejichž
základě bude vytvořen ověřený certifikát. Certifikát certifikační autority
je takzvaný self-signed, tedy ověřený sám sebou.

Zahájení bezpečné komunikace mezi serverem a klientem předchází TLS Handshake,
v rámci kterého dojde k předání certifikátů a dohodnutí na parametrech spojení.
Pro ověření důvěryhodnosti certifikátu je potřeba mít k dispozici certifikát
autority, respektive její veřejný klíč. Typicky to probíhá tak, že
certifikát je podepsán světově známou certifikační autoritou
jako je například Verisign, jejíž certifikát je součástí webových
prohlížečů, tedy klientů, kteří usilují o připojení k serverům.

I pro výše popsané testovací aplikace je potřeba dodržet tuto infrastrukturu
certifikátů. Protože se jedná jen o testování, je možné
vytvořit si lokální certifikační autoritu. K tomu účelu lze využít
aplikační vrstvu projektu OpenSSL.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{shell}
openssl genrsa -out CA.key
openssl req -x509 -new -key CA.key -out CA.pem
\end{minted}
\caption{Vytvoření self-signed certifikační aurtority}
\label{code:certCARSA}
\end{listing}

\else

\begin{verbatim}
openssl genrsa -out CA.key
openssl req -x509 -new -key CA.key -out CA.pem
\end{verbatim}

\fi

\noindent Výpis \ifdefined\ovrlf \ref{code:certCARSA} \fi vytváří
lokální certifikační autoritu. Nejdříve vytvoří soukromý klíč do \verb|CA.key| a
následně certifikát do \verb|CA.pem|. Při vykonávání druhého příkazu
se otevře možnost interaktivního doplnění informací o certifikátu, tomu lze
předejít předáním dat přes přepínač \verb|subj|.

Pro vytvoření ověřeného certifikátu pro server se postupuje následovně.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{shell}
openssl genrsa -out server.key
openssl req -new -key server.key -out server.csr
openssl x509 -req -in server.csr
                  -CA CA.pem -CAkey CA.key
                  -CAcreateserial -out server.pem
\end{minted}
\caption{Vytvoření ověřeného serverového certifikátu}
\label{code:certServerRSA}
\end{listing}

\else

\begin{verbatim}
openssl genrsa -out server.key
openssl req -new -key server.key -out server.csr
openssl x509 -req -in server.csr 
                  -CA CA.pem -CAkey CA.key
                  -CAcreateserial -out server.pem
\end{verbatim}

\fi

\noindent Nejdříve se vytvoří nový soukromý klíč do souboru \verb|server.key|. Následně
se vytvoří CSR, do jehož tvorby se opět vloží interaktivní zadávání informací o serveru.
Opět tomu lze předejít stejně jako v předchozím případě. Posledním příkazem
se pomocí dříve vytvořené certifikační autority akceptuje CSR a vytvoří certifikát
\verb|server.pem|. Stejným stylem se vytváří i certifikát klienta.

Těmito pěti příkazy byla vytvořena infrastruktura pro síťovou autorizaci pomocí RSA. Pro vygenerování ECDSA klíče je nutné specifikovat identifikátor
eliptické křivky.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{shell}
openssl ecparam -name prime256v1 -genkey -out ecdsa.key
\end{minted}
\caption{Vytvoření soukromého ECDSA klíče}
\label{code:certECDSA}
\end{listing}

\else

\begin{verbatim}
openssl ecparam -name prime256v1 -genkey -out ecdsa.key
\end{verbatim}

\fi

\noindent Generování CSR a jeho schválení je identické s předchozími příklady.
Vytvořeným serverovým certifikátům bude samozřejmě důvěřovat jen klient, který důvěřuje těmto
lokálním certifikačním autoritám a má k dispozici jejich certifikáty. Aby se využilo schopností
capi a cng modulů, je nyní potřeba umístit certifikáty do úložiště certifikátu ve Windows.

Pokud je požadováno předat certifikát úložišti a mít k němu navázán soukromý klíč,
je vhodné převést pár do formátu pkcs12, jak ukazuje příkaz \ifdefined\ovrlf \ref{code:certBundle}. \fi

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{shell}
openssl pkcs12 -export -inkey private_key.key 
                       -in certificate.pem
                       -out key_cert_bundle.pfx
\end{minted}
\caption{Transformace certifikátu a klíče do formátu PKCS12}
\label{code:certBundle}
\end{listing}

\else

\begin{verbatim}
openssl pkcs12 -export -inkey private_key.key 
                       -in certificate.pem
                       -out key_cert_bundle.pfx
\end{verbatim}

\fi

Existuje více způsobů jak pfx soubor předat úložišti. První možností je
využít přímo CryptoAPI rozhraní prostřednictvím aplikace v jazyce C nebo C\#.
Rychlejší variantou je využít příkaz \verb|certutil -importpfx|, kterému
lze specifikovat konkrétní CSP, nebo PowerShell příkaz \verb|Import-PfxCertificate|,
který ale využívá CNG, a tak uložené klíče nebudou z CryptoAPI dostupné.
Možností, která nejlépe představí certifikační úložiště, je využít grafické rozhraní
označováno jako Microsoft Management Console. Aplikaci lze spustit příkazem
\verb|certmgr| v příkazovém řádku. Ve výchozím nastavení zobrazí pouze
certifikáty přístupné aktuálnímu uživateli. Import certifikátu se
provede pomocí \verb|Action > All Tasks > Import...|,
což spustí interaktivního průvodce. Po určení cesty
k pfx souboru je možné vybrat, do kterého úložiště soubor přiřadit.
Certifikát serveru a klienta by se měl umístit do Personal, certifikát
autority by se měl importovat do Trusted Root Certification Authorities.
\newpage
\section{Ukázka dalších funkcí}

Pro prezentaci práce s nadstandardními příkazy modulu byla vytvořena aplikace
\verb|eng_load.c|. Ta očekává předání cesty ke sdílené knihovně s kryptografickým
modulem jako první argument. K jeho načtení je použit dynamic engine, stejně 
jako tomu je u předchozích aplikací. Aplikace nejdříve vypíše celou nabídku nadstandardních
příkazů a vybrané z nich vykoná.

Kromě příkazů, které si engine definuje sám, existuje i sada obecných příkazů.
Protože je nutné tyto příkazy volat přímo přes jejich číselný kód, je ze tří
rozhraním nabízených funkcí přípustná pouze \verb|ENGINE_ctrl()|. 
Aplikace implementuje funkci \verb|print_ctrls()|, která využívá obecné
funkce pro vypsání všech dostupných specifických příkazů.

Aplikace dále prezentuje volání příkazů modulu pomocí jejich jmenného identifikátoru.
K tomu Engine API vyhrazuje funkci \verb|ENGINE_ctrl_cmd()| nebo \verb|ENGINE_ctrl_cmd_string()|.
Ta druhá přináší vyšší míru abstrakce, protože není třeba znát datový typ, který příkaz
očekává. Funkce to sama ověří a pokud je to nutné, datový typ převede. Poslední
argument těchto funkcí rozhoduje, jestli má být ignorována případná neexistence požadovaného
příkazu. Následující ukázka prezentuje použití obou funkcí pro přesměrování
debugovacích hlášek.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
/*e je reference struktury ENGINE*/
ENGINE_ctrl_cmd_string(e, "debug_file", "trace.log", 0);
ENGINE_ctrl_cmd(e, "debug_level", 2, NULL, NULL, 0);
\end{minted}
\caption{Volání nadstandardních příkazů}
\end{listing}

\else

\begin{verbatim}
/*e je strukturální reference ENGINE*/
ENGINE_ctrl_cmd_string(e, "debug_file", "trace.log", 0);
ENGINE_ctrl_cmd(e, "debug_level", 2, NULL, NULL, 0);
\end{verbatim}

\fi

\section{Pozorování}

Pro otestování funkčnosti kryptografického modulu byl využit testovací server a klient
za použití certifikátů a klíčů vygenerovaných metodou, která byla popsána v kapitole \ref{sec:certs}.
Server i klient je nastavitelný pomocí konfiguračního souboru vstupujícího
jako první argument při spouštění aplikace. Postup pro přípravu testovacího prostředí a způsob
jak testovací aplikace spouštět a konfigurovat je popsán v souboru README.txt na přiloženém
datovém úložišti.

Testovací sada byla nejdříve vyzkoušena na ověřeném capi modulu za použití RSA certifikátů.
Nejdříve byl klient omezen tak,
aby umožnil spojení pouze pomocí protokolu TLS 1.0. V tom případě se spojení uskutečnilo
a klient zobrazil přijatou zprávu. Stejný výsledek mělo i spojení řízené protokolem
TLS~1.1. Na těchto dvou verzích v pořádku proběhla i autorizace klienta. Protože
byl capi modul zkompilován s příznakem \verb|OPENSSL_CAPIENG_DIALOG|, po obdržení
Certificate Requestu se zobrazilo dialogové okno pro výběr certifikátu klienta.
Při použití novějších protokolů se zobrazila chybová hláška
o chybějící implementaci funkce pro šifrování soukromým klíčem, jak bylo popsáno v kapitole
\ref{sec:capiErr}. Tuto chybu lze obejít tak, že nastavením spojení bude vynuceno použití podpisového algoritmu,
který nepracuje se schématem PSS, například \verb|RSA+SHA256|. Poté bylo možné spojení
uzavřít i s použitím aktuálních protokolů a přijmout zprávu ze strany serveru.

Podobně byla otestována i funkčnost cng modulu. Server byl spuštěný tak, aby využíval
kryptografický modul cng. Pokud je vedle toho spuštěn klient bez jakéhokoliv omezení,
dohodnuté spojení dodržuje protokol TLS 1.3 s šifrovou sadou \verb|TLS_AES_256_GCM_SHA384|.
Kromě obdržené zprávy jsou dále vypsány informace o serverovém certifikátu, v tomto případě
se jedná o certifikát podepsaný RSA klíčem. Pokud klient omezí podmínky komunikace
na protokol TLS~1.2, uzavře se spojení s šifrovou sadou \verb|ECDHE-ECDSA-AES256-GCM-SHA384|,
využije se tedy ECDSA algoritmus implementovaný modulem. Upravením klientem nabízené šifrové sady
lze vynutit použití RSA algoritmu, jenž taktéž úspěšně skončí vypsáním zprávy od serveru.
Nic na tom nezmění ani zákaz RSA-PSS. Klientova reakce na zprávu Certificate Request
je stejná jako v případě capi. Pokud je modul kompilován s příznakem
\verb|OPENSSL_CNGENG_DIALOG|, zobrazí se dialogové okno, přes které lze interaktivně
zvolit certifikát, který bude zaslán serveru. Stejný postup testování byl aplikován
i při využití aktuálně vyvíjené verze OpenSSL~3.0, výsledky byly identické.

Pro úplnost byla otestována i zpětná kompatibilita modulu cng. Při využití
algoritmu RSA společně se zastaralými protokoly TLS 1.0 nebo 1.1 nelze uzavřít spojení.
Je to způsobeno tím, že tyto verze si pro hešování
vyžádají algoritmus SHA+MD5, tedy SHA následovaný MD5, což Cryptography API: Next Generation
ve svém základu nepodporuje. Pomocí algoritmu ECDSA lze ale zahájit komunikaci
ve všech verzích protokolu TLS.

Při pokusu zkompilovat modul pomocí knihoven OpenSSL~1.0 nastane několik chyb.
To je způsobeno tím, že s OpenSSL 1.1 bylo vytvořeno
nové rozhraní kryptografických struktur. Modul cng používá struktury \verb|EC|, zatímco ve starší verzi
OpenSSL byly pojmenovány \verb|ECDSA|. Také přibylo několik get funkcí, které dříve neexistovaly.
Tyto rozdíly by bylo možné vyřešit podmíněným překladem, avšak OpenSSL 1.0 není již dále podporovanou verzí,
a proto se engine soustředí na funkčnost v aktuální LTS verzi.
