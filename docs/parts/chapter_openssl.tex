\chapter{OpenSSL}


Projekt OpenSSL je balík open source nástrojů pro podporu TLS a SSL protokolů
a s tím spojené kryptografické operace. Produkt podléhá licenci Apache a lze
jej tedy plně využívat i pro komerční účely. V projektu převládá programovací
jazyk C. Implementace se striktně drží standardů popsaných v~RFC.

První verze byla vytvořena v~roce 1998 skupinou vývojářů \cite{opensslYears}. Nyní existuje
osmnáct přispěvatelů s~právem přímo upravovat projekt. V roce 2017 vstoupila
v platnost interní směrnice \cite{osslBylaws}, která jasně definuje
hierarchii přispěvatelů. Management Committee definuje požadavky na~produkt
a~rozhoduje o~strategických směrech, členové Technical Committee zastávají
funkci softwarových architektů. Členství v~těchto kruzích se~ze~značné části
překrývá.

OpenSSL je poskytovatelem knihoven libssl a~libcrypto, které lze využívat
pomocí API. Libssl dokáže zprostředkovat šifrovanou komunikaci mezi serverem
a~klientem pomocí protokolů SSL nebo TLS. Libcrypto nabízí široké spektrum
kryptografických operací, které je navíc rozšiřitelné pomocí kryptografických
modulů. Bližšímu popisu architektury se~věnuje kapitola \ref{sec:struktura}.

Vedle knihoven do~balíku nástrojů zapadá program \cite{osslman1} ovládaný
přes příkazovou řádku, s~jehož pomocí lze přímo vykonávat následující
kryptografické operace:
\begin{itemize}
    \item Tvorba a~správa soukromých a~veřejných klíčů.
    \item Vykonávání operací asymetrické kryptografie.
    \item Vytváření certifikátů, CSR a~CRL.
    \item Vytváření heše zprávy.
    \item Šifrování a~dešifrování zprávy.
    \item Testování spojení serveru a~klienta.
    \item Manipulace s emaily chráněnými protokolem S/MIME.
\end{itemize}


\section{Alternativy}
Implementace protokolů SSL a~TLS není výsadou OpenSSL. Vedle této kniho\-vny existují
jak další open source projekty, tak proprietární software. V době vzniku
této práce většina velkých projektů disponovala podporou TLS 1.3, výjimku
tvoří například SChannel využívaný v systémech Windows. Níže je stručný popis
knihoven, které se často uvádějí jako vhodné alternativy k OpenSSL.

LibreSSL vznikl v roce 2014 jako odnož OpenSSL krátce poté, co byla odhalena
vážná bezpečnostní hrozba Heartbleed. Software vzniká pod záštitou projektu
OpenBSD a jeho hlavním cílem \cite{libressl} je zpřehlednit zdrojové kódy,
zbavit se zastaralých nebo nefunkčních částí a zmodernizovat celou metodiku
vývoje. Hned v~první verzi byl z důvodu závislosti na nesvobodném software
odstraněn modul pro podporu Microsoft CryptoAPI.

WolfSSL rovněž vznikl kvůli nespokojenosti s~již existujícím OpenSSL. Tvůrci
dávají důraz na rychlost, malou velikost a~přenositelnost. Knihovna wolfSSL
je údajně
\cite{wolfssl} dvacetkrát menší než implementace od OpenSSL. Obdobnou
strategii minimalistického řešení volí i~MatrixSSL \cite{matrixssl}. Díky své
malé náročnosti na paměť najdou knihovny využití ve~vestavěných systémech a
IoT zařízeních. Přechod z~OpenSSL ulehčují nadstavbou pro snazší portaci.

BoringSSL je oddělenou větví od OpenSSL, je určena čistě pro potřeby produktů
Google a~neklade si za cíl proniknout do~širší veřejnosti. Hned v~prvním
odstavci dokumentace \cite{boringssl} nalezneme varování vývojářů, že~nelze
garantovat stabilitu při používání jejich API. Vývojáři totiž nevěnují
pozornost zpětné kompatibilitě. Značná část struktury je stále s~OpenSSL
shodná.

GnuTLS v~přehledu \cite{gnutls} sděluje, že se jedná o~jednoduchou
implementaci TLS protokolů v~jazyce C tak, aby se dokázala vmísit mezi již
existující linuxové knihovny. Dělí se do tří nezávislých částí. Jedna část je
samotná implementace TLS protokolu, ta tvoří hlavní jádro projektu.
Kryptografický back-end a~část pro práci s~certifikáty pak využívá jiné
knihovny GNU projektu.

Existují i~knihovny, které nejsou vytvořeny v~programovacím jazyce C. Rustls,
jak již název napovídá, je vytvořen v~Rust. Zajímavostí je, že ačkoliv celý
projekt udržuje jediný vývojář \cite{rustls}, je projekt stále aktuální.
Dalším příkladem budiž Java Secure Socket Extension pod dohledem firmy
Oracle.

Apache HTTP Server a Nginx jsou nejvyužívanějším softwarem pro provoz
webových serverů, spolu tvoří přibližně 70 \% webových serverů
\cite{w3techStats}. Z dokumentace Nginx \cite{nginx} vyplývá, že ke
konfiguraci HTTPS serveru je potřeba OpenSSL. Apache nabízí možnost výběru
mezi OpenSSL a WolfSSL. Nová verze WolfSSL 4.2.0 \cite{wolfsslApache} totiž v
lednu 2020 přišla s podporou Apache HTTP Serveru.

Evidentně existuje několik zajímavých alternativ, některé z nich poukazují na
nedostatky OpenSSL a~přichází s odlehčenější strukturou. Žádná z~nich nemá
tak bohatou komunitu, jako je tomu u~OpenSSL. Tato práce je
zaměřená na~vytvoření modulu pro zapojení Crypto API Next Generation
do~implementace TLS protokolu a~k~tomu je přizpůsobena právě knihovna
OpenSSL.


\section{Architektura} \label{sec:struktura}
OpenSSL verze 1.1.1 je strukturovaná do čtyř vrstev \cite{osslstrategy}. Za
jádro projektu lze považovat kryptografickou vrstvu, tu reprezentuje knihovna
libcrypto. Obsahuje implementace kryptografických algoritmů a může být
volitelně rozšířena o další funkcionalitu pomocí doplňkového modulu,
oficiální dokumentace ho nazývá jako engine a považuje jej z hlediska
architektury za zvláštní komponentu.

Další část utváří TLS komponenta, ta popisuje roli knihovny libssl.
Implmentuje SSL, TLS a DTLS protokoly dle aktuálně definovaných standardů a
je závislá na funkcích knihovny libcrypto. Nejvyšší vrstvu představuje
rozhraní pro aplikace spustitelné v příkazovém řádku, ke své činnosti
potřebuje obě zmiňované knihovny.

Diagram na obrázku \ref{fig:osslStruct} zachycuje architekturu OpenSSL
a vnitřní komponenty knihoven, zredukované
na prvky, které  byly využity při tvorbě kryptografického modulu.
%tato práce popisuje v následujících kapitolách.
% Znázornění kryptografické vrstvy je naopak obohaceno o podrobnější vhled do logických celků.

\begin{figure}[!htbp]\centering
\begin{tikzpicture}[
    module/.style={%
        draw, rounded corners,
        minimum width=#1,
        minimum height=7mm,
        % font=\sffamily
        },
	module/.default=2cm,
	module_small/.style={%
        draw, rounded corners,
        minimum width=#1,
        minimum height=7mm,
        % font=\sffamily
		},
    module_small/.default=1cm,
    module_label/.style={%
        % draw,
        anchor=south west,
        minimum width=#1,
        minimum height=6mm,
        % font=\sffamily
		},
    module_label/.default=2.2cm,
    module_crypto/.style={%
        draw, rounded corners,
        anchor=south west,
        minimum width=38mm,
        minimum height=7mm,
        % font=\sffamily
		},
    module_crypto/.default=2cm,
    module_medium/.style={%
        draw, rounded corners,
        anchor=south west,
        minimum width=20,
        minimum height=28mm,
        % font=\sffamily
		},
    module_medium/.default=2cm,
    module_bio/.style={%
        draw, rounded corners,
        anchor=north west,
        minimum width=20mm,
        minimum height=26.5mm,
        % font=\sffamily
		},
    module_bio/.default=2cm,
]
    
    \node[module] (app_enc) {enc};
    \node[module_label, above=0.3mm of app_enc,
          text width=3.8cm, align=flush left, 
          xshift=9.2mm] (app_label) {uživatelsky spustitelné};
    \node[module, right=2mm of app_enc] (app_dgst) {dgst};
    \node[module, right=2mm of app_dgst] (app_ciphers) {ciphers};
    \node[module, right=2mm of app_ciphers] (app_req) {req};
    \node[module, below=2mm of app_enc] (app_genrsa) {genrsa};
    \node[module, right=2mm of app_genrsa] (app_ecparam) {ecparam};
    \node[module, right=2mm of app_ecparam] (app_pkcs12) {pkcs12};
    \node[module, right=2mm of app_pkcs12] (app_dots) {\dots};
    \node[fit=(app_enc) (app_dots), draw, inner sep=1mm] (app) {};

    \node[module, below=8mm of app_genrsa] (tls_tls) {SSL};
    \node[module_label, above=0.3mm of tls_tls,
          text width=1.9cm, align=flush left] (tls_label) {libssl};
    \node[module, right=3mm of tls_tls] (tls_dots) {\dots};
    % \node[module, below=3mm of tls_tls] (tls_statem) {STATEM};
    % \node[module, right=3mm of tls_statem] (tls_dots) {\dots};
    \node[fit=(tls_tls) (tls_dots), draw, inner sep=1mm] (tls) {};

    
    \node[module, below=22mm of app] (cr_engine) {
        \begin{tabular}{ r l }
             \multicolumn{2}{l}{\tiny{engine}} \\ 
              & capi \\
              & dynamic \\
              & \dots
        \end{tabular} 
    };

    \node[module, left=2mm of cr_engine] (cr_evp) {
        \begin{tabular}{ r l }
             \multicolumn{2}{l}{\tiny{evp}} \\
              & EVP\_PKEY \\ 
              & EVP\_MD \\ 
              & EVP\_CIPHER \\ 
        \end{tabular}
    };
    \node[module_label, above=0.3mm of cr_evp,
          text width=3.5cm, align=flush left] (cr_label) {libcrypto};

    \node[module, right=2mm of cr_engine] (cr_certs) {
        \begin{tabular}{ r l }
             \multicolumn{2}{l}{\tiny{certifikátové struktury}} \\ 
              & X509 \\
              & PEM \\
              & \dots
        \end{tabular} 
    };

 
    \node[module_medium, below=2mm of cr_engine, xshift=4mm] (cr_lowlevel) {
       \begin{tabular}{ r l }
             \multicolumn{2}{l}{\tiny{kryptografické struktury}} \\ 
              & RSA \\ 
              & DSA \\
              & EC\_KEY \\
              & \dots
        \end{tabular} 
    };

    \node[module_medium, left=2mm of cr_lowlevel] (cr_buffer) {
        \begin{tabular}{ r l }
             \multicolumn{2}{l}{\tiny{pomocné struktury}} \\ 
              & CRYPTO \\ 
              & BUFFER \\
              & BN \\
              & \dots
        \end{tabular}
    };
   
    \node[module_medium, right=2mm of cr_lowlevel] (cr_bio) {
        \begin{tabular}{ r l }
             \multicolumn{2}{l}{\tiny{i/o operace}} \\ 
              & BIO \\
              & \\ & \\ &
        \end{tabular} 
    };

    % \node[module, right=3mm of cr_buffer] (cr_engine) {ENGINE};
    % \node[module, right=3mm of cr_engine] (cr_dots) {\dots};
    \node[fit=(cr_evp) (cr_certs) (cr_lowlevel), draw, inner sep=1mm] (cr) {};

    % \node[module_small, below=8mm of cr_engine] (eng_cng) {cng};
    % \node[module_small, left=3mm of eng_cng] (eng_capi) {capi};
    % \node[module_small, right=3mm of eng_cng] (eng_dots) {\dots};
    % \foreach \i in {cng,capi,dots}
    %     \draw[-Latex] (cr_engine)--(eng_\i.90);

    \draw[Latex-] (tls.90) -- +(0mm,6mm);
    \draw[-Latex] (tls.270)-- +(0mm,-6mm);
    \draw[-Latex] (app.320)-- +(0mm,-21mm);


\end{tikzpicture}

\caption{Zjednodušený diagram struktury OpenSSL}
\label{fig:osslStruct}

\end{figure}


\FloatBarrier

\section{Knihovna libcrypto}

Knihovna libcrypto přináší implementace kryptografických operací.
Jak je naznačeno na obrázku \ref{fig:osslStruct}, knihovna samotná
se dělí do několika komponent. Je pravidlem, že každá z komponent
poskytuje své rozhraní, které je definované v hlavičkových souborech
v adresáři \path{include/openssl/}. Implementace jednotlivých struktur
nebo funkcí jsou v projektu umístěny pod adresářem \path{crypto/}.
Následující kapitoly se věnují komponentám, které byly přímo využity
při tvorbě kryptografického modulu a jejichž funkcionalita by měla
být vysvětlena.


\subsection{Kryptografické struktury}

Každý kryptografický algoritmus implementovaný v OpenSSL má svoji
nízko\-úrovňovou reprezentaci. Typicky to znamená, že existuje nezávislá
struktura, která zabaluje funkce nezbytné k provedení daných kryptografických
operací, dodatečné parametry a případně i klíče.
Přímé přístupy k těmto strukturám jsou zastíněny rozhraními, která jsou deklarovány
přís\-lu\-šnými hlavičkovými soubory.

Struktura \verb|RSA| se skládá z několika \verb|BIGNUM| komponent pro
uchování modulu, exponentů a dalších parametrů využívaných v RSA
algoritmech. Slouží tedy pro reprezentaci soukromých a veřejných klíčů.
Struktura mimo to obsahuje \verb|RSA_METHOD|, nositele implementací
RSA operací.

\verb|RSA| strukturu lze inicializovat pomocí \verb|RSA_new_method()|, jejíž
argumentem může být ukazatel na nějaký \verb|ENGINE|. V tom případě se při
volání RSA operací využije \verb|RSA_METHOD| implementovaná v příslušném modulu.
Pokud je argumentem \verb|NULL|, bude namísto toho zvolena defaultní
implementace. Dosadit lze samozřejmě i instanci \verb|RSA_METHOD| přímo,
v tom případě musí následovat přenastavení až po inicializaci pomocí
\verb|RSA_set_method()|, která zapříčiní uvolnění dříve přiděleného
modulu. 

Struktura \verb|RSA_METHOD| \cite{osslrsaMethod} předepisuje funkce pro RSA
operace. Úkolem \verb|RSA_public_encrypt()| a
\verb|RSA_private_decrypt()| je šifrování veřejným klí\-čem a dešifrování
soukromým klíčem. \verb|RSA_private_encrypt()| má dle dokumentace
\cite{osslrsaPrivEnc} za úkol zvládat vytvoření digitálního podpisu předem zahešovaných dat a
\verb|RSA_public_decrypt()| ověřová\-ní podpisu. Všechny tyto funkce spojují
vstupní argumenty. Očekávají data ke zpracování, jejich velikost, výstupní
paměťový blok s dostatečnou velikostí, strukturu \verb|RSA| a použitý
padding. Od toho se odlišují funkce \verb|RSA_sign()| a~\verb|RSA_verify()|,
které jsou obohacené informací o hešovacím algoritmu použi\-tém před samotným
podepsání. Naproti tomu jsou funkce ochuzené o možnost specifikovat padding.
To v současné době, kdy se používají dvě různá podpisová schémata RSA, nemusí
být vyhovující řešení. Avšak pro obejití nedostatku lze využít právě
\verb|RSA_private_encrypt()| nebo \verb|RSA_public_decrypt()|. Poslední
funkce \verb|rsa_mod_exp()| a \verb|bn_mod_exp()| jsou využívány interně
pro výpočet mocniny v konečném tělese.

Pro DSA operace je připravená struktura \verb|DSA|. Je rovněž složena ze sady
\verb|BIGNUM| komponent pro reprezentaci klíčů a odkazu na \verb|DSA_METHOD|.
Systém inicializace je stejný, ale formát kryptografických funkcí se značně
liší. Funkce \verb|DSA_sign_setup()| pro výpočet \(k^{-1}\) a \(r\) způsobí
jen zpomalení výpočtů, existuje už jen kvůli zpětné kompatibilitě a neměla by
být používána \cite{ossldsaSign}. Funkce \verb|DSA_do_sign()| vrátí digitální podpis
předaných dat jako strukturu \verb|DSA_SIG|, což reprezentuje dvojici 
\verb|(r, s)|. Je to jediná funkce ze všech výše zmiňovaných, která nemá návratový
 typ celé číslo pro ověření správnosti běhu.

V případě protokolu digitálního podpisu s využitím eliptických křivek
nalezneme odlišností více. Struktura \verb|EC_GROUP| definuje
eliptickou křivku. Body na křivce reprezentuje struktura \verb|EC_POINT|.
\verb|EC_KEY| pak slouží k uchování klíčů, soukromý klíč je definován pomocí
\verb|BIGNUM|, veřejný pomocí \verb|EC_POINT|. Implementace
jednotlivých operací zastřešuje struktura \verb|EC_METHOD|.

\verb|EC_KEY| se inicializuje funkcí \verb|EC_KEY_new_by_curve_name()|, které
se pře\-dá číselný identifikátor příslušné křivky. Seznam podporovaných křivek
v instanci OpenSSL lze získat příkazem \verb|openssl ecparam -list_curves| v
příkazovém řádku. Uvedená inicializační funkce jen obaluje zavolání posloupnosti
dvou funkcí pro alokaci paměti a pro přidělení křivky.

K vytvoření digitálního podpisu se nabízí několik \cite{osslecdsaSign} kryptografických funkcí.
\verb|ECDSA_do_sign_ex()| očekává vstupní data, již alokovaný výstupní
buffer, strukturu \verb|EC_KEY| a volitelně i \(k^{-1}\) a \(r\) pro
urychlení výpočtů. Obdobně funguje i \verb|ECDSA_sign_ex()| s tím rozdílem,
že podpis bude vrácen strukturou \verb|ECDSA_SIG| jako návratová hodnota.
Funkce \verb|ECDSA_sign_setup()| slouží ke spočítání dvou výše zmíněných
hodnot pro urychlení procesu podepisování.

Nízkoúrovňové struktury pro symetrickou kryptografii a hešování jsou reprezentovány
na podobném principu. Každý algoritmus a každý mód má svoji strukturu, tedy
balík funkcí a parametrů nezbytné k vykonávání příslušných operací v
příslušném hlavičkovém souboru \verb|\include\openssl\|.
Pro všechny takové struktury platí, že by v aplikaci neměly být využívány,
a namísto toho kryptografické operace provádět prostřednictvím EVP API.

\subsection{EVP}
Envelope, zkráceně EVP, je vysokoúrovňové rozhraní pro kryptografické operace
jako je šifrování a dešifrování, podepisování a ověřování nebo hešování. Z
pohledu architektury leží nad strukturami z předchozí kapitoly a rozdělují se
do tří hlavních objektů \cite{osslCodeEvph}.

Struktura \verb|EVP_CIPHER| představuje jeden konkrétní mód nebo
algoritmus pro symetrické šifrování. Obsahuje identifikátor
šifry, výchozí velikost klíče, bloku nebo inicializačního vektoru.
Dále už následují funkční ukazatele, jimž je přiřazena implementace
dané operace. Jádrem \verb|EVP_CIPHER| jsou funkce namapované na
\verb|*do_cipher| a \verb|*init|, které typicky vyústí
ve využití metod příslušné nízkoúrovňové kryptografické struktury.
K \verb|EVP_CIPHER| aplikace nepřistupuje přímo, ale prostřednictvím
kontextové struktury \verb|EVP_CIPHER_CTX|, která se inicializuje
pomocí \verb|EVP_EncryptInit()|. V ten moment je kontextové
struktuře přiřazena \verb|EVP_CIPHER|, popřípadě další data jako
klíč nebo inicializační vektor. Volitelně lze určit i konkrétní
engine, který bude pro výpočty využit. Pro následující šifrovací operace,
využívá aplikace pouze kontextovou strukturu. V případě EVP API
je dokumentace \cite{osslEvpCipher}
velice sdílná, nabízí ukázku šifrování zprávy pomocí pomocí EVP API.

\verb|EVP_MD| je určena pro reprezentaci hešovacích funkcí.
Obdobně, jako v předchozím případě, existuje příslušná kontextová struktura
\verb|EVP_MD_CTX| a funkce z rozhraní EVP API s prefixem \verb|EVP_Digest|.
Jejich aplikační použití a architektonická stavba je shodná s šifrovacími
strukturami \cite{osslCodeEvph}, a proto nebude podrobněji rozebírána. 

S asymetrickou kryptografií přichází změna v názvosloví. Schránku
funkcí netvoří struktura \verb|EVP_PKEY|, nýbrž \verb|EVP_PKEY_METHOD|.
Ve té jsou připraveny ukazatele na operace
spojené s asymetrickou kryptografií. Pro vytvoření digitálního podpisu
jsou připraveny hned dvě funkce. Rozdílem je, že \verb|*digestsign|
oproti \verb|*sign| neočekává na vstupu již zahešovaná data.
Opět existuje kontextová struktura \verb|EVP_PKEY_CTX|, jež
plní stejný účel jako u výše jmenovaných. Obsahuje \verb|EVP_PKEY_METHOD|
a \verb|EVP_PKEY| spolu s dalšími informacemi a daty potřebnými k uskutečnění
požadovaných operací.

Struktura \verb|EVP_PKEY| uchovává soukromý nebo veřejný klíč.
Jeho položka \verb|type| určuje identifikátor algoritmu, s kterým
je spojen klíč \verb|pkey|, reprezentovaný formou
příslušné struktury z předchozí kapitoly,
tedy \verb|RSA|, \verb|DSA|, \verb|EC_KEY| a podobně \cite{osslCodeCryptoEvph}.
Jedná se tedy opět o abstrakci nízkoúrovňových struktur.

OpenSSL poskytuje asi osmnáct \cite{osslCodePmethLib}
různých \verb|EVP_PKEY_METHOD|, například RSA, DSA nebo ECDSA algoritmů. 
Vnitřně jsou tyto struktury uchovávány v poli a lze do něj přidat
další specifické implementace.

EVP je komplexní rozhraní, zastiňuje používání kryptografických
struktur představené v předchozí kapitole a ulehčuje tak použití
celé knihovny. Pomocí EVP API lze snadno přistupovat ke všem výše
popsaným strukturám. V aplikacích je hojně používané
a dokumentací podrobně popisované.


% \subsection{Certifikátové struktury}

% OpenSSL implementuje struktury pro reprezentaci certifikátů.
% Struktura \verb|X509| seskupuje data dle standardu RFC 3280 
% pro certifikáty X.509 a poskytuje rozhraní umožňující snazší 
% manipulaci s těmito objekty.
% \verb|d2i_X509()| vytvoří \verb|X509| strukturu z 


\subsection{Pomocné struktury}

Knihovna libcrypto implementuje mimo jiné algoritmy, jejichž bezpečnost je založena na
aritmetice velkých čísel. Pro tento účel je vhodné použít strukturu
\verb|BIGNUM|, která k uchovávání čísla využívá dynamické alokace a tudíž není
limit pro velikost \cite{osslBn}. Při práci s touto strukturou je však nutné
věnovat zvýšenou opatrnost, zda nedošlo k chybě při alokování prostředků.
\verb|BIGNUM| poskytuje rozhraní, s jehož pomocí lze provádět standardní
aritmetické operace. Implementační část práce přímo využívá tuto strukturu
pro reprezentaci klíčů. Funkce \verb|BN_bin2bn()| převede kladné celé číslo v
big-endian podobě do nově alokované \verb|BIGNUM|.

\verb|CRYPTO| je asi nejrozsáhlejší pomocnou strukturou. Její hlavní síla je v implementaci
funkcí pro správu paměti a vláken, také provádí inicializace dalších struktur.
Z této nabídky se při tvorbě kryptografického modulu uplatnily pouze funkce
\verb|OPENSSL_malloc()| a \verb|OPENSSL_free()|, jež jsou abstrakcemi pro
známé funkce z jazyka C.

Vybraným OpenSSL strukturám je umožněno připnout k sobě libovolná data,
která by se mohla hodit v daném případě užití. Možnost je otevřena
nízkoúrovňovým kryptografickým strukturám jako je \verb|RSA|, \verb|DSA|,
\verb|EC_KEY| a komponentě \verb|ENGINE|. Dokumentace tato přídavná data
označuje jako \verb|exdata|. 

\verb|Exdata| jsou jednoznačně identifikována indexem, který je výstupem
volání \verb|CRYPTO_get_ex_new_index()| na začátku aplikace \cite{osslExdata}.
Pro tuto obecnou funkci jsou vytvořená makra ke každé struktuře, která ulehčí její
využívání.
Jako argument lze předat callback tří funkcí, které budou vyvolány
při vzniku, uvolnění nebo kopírování původní struktury.
Získaný index se typicky
uchová ve formě globální proměnné. Pro zápis nebo čtení z
\verb|exdata| je potřeba využít funkce poskytované konkrétní strukturou,
uvolnění dat proběhne nastavením položky \verb|exdata| na \verb|NULL|.

\ifdefined\ovrlf
\begin{listing}[!h]
\begin{minted}{c}
RSA *rsa = RSA_new();
int rsa_ex_idx;

rsa_ex_idx = RSA_get_ex_new_index(0, NULL, NULL, NULL, NULL);
RSA_set_ex_data(rsa, rsa_ex_idx, "Save this string.");
RSA_get_ex_data(rsa, rsa_ex_idx);
RSA_set_ex_data(rsa, rsa_ex_idx, NULL);
\end{minted}
\caption{Příklad využití exdat bez callback funkcí}
\end{listing}

\else

\begin{verbatim}
RSA *rsa = RSA_new();
int rsa_ex_idx;

rsa_ex_idx = RSA_get_ex_new_index(0, NULL, NULL, NULL, NULL);
RSA_set_ex_data(rsa, rsa_ex_idx, "Save this string.");
RSA_get_ex_data(rsa, rsa_ex_idx);
RSA_set_ex_data(rsa, rsa_ex_idx, NULL);
\end{verbatim}

\fi


\subsection{Vstupní a výstupní operace}

OpenSSL poskytuje obecnou strukturu pro zpracování vstupních a výstupních
operací. \verb|BIO|, jak je struktura pojmenovaná, není omezená na standardní
file stream operace, dokáže zpracovat i komunikaci mezi serverem a klientem \cite{osslBio}.
Jednotlivé struktury lze totiž řetězit, což otevírá možnost provádění
transformací nad protékajícími daty.

\verb|BIO| je velmi silnou abstrakcí, která odstiňuje všechny ostatní
komponenty knihovny.
Nešifrovanou komunikaci serveru a klienta je možné snadno navázat pomocí
\verb|BIO_new_connect()|, jejíž vstupem je jen adresa a port. Číst a psát do streamu
umožňují funkce \verb|BIO_read()| a \verb|BIO_write()|. Uskutečnění šifrované spojení
vyžaduje zavolání několika funkcí navíc a strukturu \verb|SSL|,
té je potřeba specifikovat verzi protokolu TLS a cestu k certifikátům.

\subsection{Engine} \label{sec:osslEngine}
Engine, nebo také kryptografický modul, umožňuje rozšířit funkcionalitu
libcrypto přes rozhraní Engine API. Typickým užitím je nahrazení defaultní
implementace kryptografických funkcí. Pokud tedy máme k dispozici knihovnu
třetí strany, můžeme její výhody přivést do OpenSSL bez zásahu do již
existujících řešení. Mějme například hardware, který dokáže urychlit
kryptografické operace. Výrobce samozřejmě k zařízení poskytne nezbytné
ovladače pro správnou funkci v běžném užití, ale nebude distribuovat instanci
OpenSSL. Ani ze strany OpenSSL nepřijde přímá podpora pro toto specifické zařízení
uvnitř libcrypto. Engine API nám však pomůže namapovat volání OpenSSL na funkce
našeho zařízení, rozhraní můžeme chápat jako kompatibilní vrstvu pro přechod
libovolné knihovny do libcrypto.

V roce 2000 existovalo OpenSSL 0.9.6 ve dvou verzích, jedna standardní a
druhá s podporou engine. Až verze 0.9.7 přišla se sjednocením a Engine API se
tak stalo nedílnou součástí projektu. V současné verzi je v OpenSSL
implementováno devět kryptografických modulů, v projektu \cite{osslGithub}
jsou rozděleny do dvou adresářů.

V adresáři \path{crypto/engine/} nalezneme čtyři moduly. Devcrypto podporuje
kryptografické pseudozařízení \verb|/dev/crypto| operačního systému OpenBSD.
Modul rdrand vytváří kompatibilní vrstvu pro hardwarový generátor náhodných
čísel od Intelu, respektive jeho instrukci pro získání náhodného čísla. Engine
openssl je v prvním komentáři zdrojového kódu označen jako \uv{testing gunk},
implementuje RC4 a ostatní operace jsou delegovány na defaultní implementace,
pravděpodobně tedy vznikl za účelem testování Engine API. Posledním je
dynamic engine, který se od všech ostatních velmi odlišuje. Neplní klasickou
funkci kryptografického modulu, jeho účel bude vysvětlen později.

Druhý adresář \path{engines/} vznikl až v pozdější verzi a skrývá zbylých pět
modulů. Engine afalg byl vytvořen pro podporu linuxového crypto API. Capi engine byl
vytvořen pro podporu Microsoft CryptoAPI, podrobnějšímu popisu se věnuje tato
práce v kapitole \ref{sec:capi}. Modul padlock umožňuje kompatibilitu CPU s
hardwarovou podporou kryptografických funkcí od firmy VIA Technologies.
Engine dasync, zkratka pro sousloví Dummy Async, prezentuje asynchronní
volání operací. Všechny jeho kryptografické funkce jsou však delegovány na
defaultní implementace, je určen pouze k testování. Poslední modul ossltest
má záměrně oslabené šifrovací funkce a je určen pouze pro testovací účely.

Není pravidlem, že se při kompilaci vytvoří všechny dostupné moduly. Záleží
na konfiguraci prostředí, pro které se knihovna kompiluje a na parametrech
makefile. Moduly, které byly staticky linkované s knihovnou libcrypto, lze
vypsat v příkazovém řádku příkazem \verb|openssl engine|. Engine API umožňuje
využívat i moduly, které v době kompilace neexistovaly, a to pomocí
speciálního modulu dynamic.
\newpage
Možnost využívat moduly, které nejsou součástí OpenSSL, přináší řadu výhod.
Například v situaci, kdy je potřeba upravit již existující s OpenSSL
propojenou implementaci modulu, není nutné znovu kompilovat celou kniho\-vnu.
Stejně tak se dynamické linkování hodí, když OpenSSL neposkytuje engine ke
specifickému zařízení. Můžeme pak využít engine poskytnutý výrobcem nebo si
vytvořit vlastní. Dynamické linkování nám také díky svojí menší náročností na
paměť pomůže s vytvořením méně nákladného řešení. Jak tyto struktury fungují,
vysvětluje kapitola \ref{sec:engine}.

\section{Knihovna libssl}

Knihovna libssl implementuje protokoly SSL, TLS a DTLS. Poskytuje rozhraní
SSL API, které je definované v hlavičkovém souboru \path{openssl/ssl.h}.
Vnitřně využívá konstrukty definované v knihovně libcrypto, například
rozhraní EVP. Podobně jako je tomu u libcrypto, i tato knihovna se dělí
do několika komponent reprezentovaných jako struktury s vlastním rozhraním.

První strukturou, kterou je nutné vytvořit pro zahájení bezpečné komunikace,
je \verb|SSL_CTX|. Tato struktura se využívá na straně klienta i serveru,
existuje po celou dobu života aplikace a specifikuje budoucí spojení.
Inicializační funkce \verb|SSL_CTX_new()| vytvoří novou strukturu a přidělí
jí seznam šifer, callback funkce a výchozí nastavení parametrů.
Funkci se jako argument musí předat \verb|SSL_METHOD|, která specifikuje
implementace funkcí spojené s požadovanou verzí protokolu.

S OpenSSL je dodáno několik implementací struktury \verb|SSL_METHOD| a SSL
rozhraní nabízí dva způsoby, jak je využívat. První možností je získání
konkrétní verze protokolu, k tomu slouží například funkce \verb|TLSv1_1_method()|.
Druhou a doporučovanou \cite{osslSslCtx} možností je využít metodu
\verb|TLS_method()|, ta vybere nejvyšší společnou verzi mezi serverem a klientem.
Automatizovaná volba jde omezit zdola i shora upravením parametrů kontextové struktury.

Vytvořené spojení klienta se serverem je reprezentované strukturou \verb|SSL|.
Inicializaci lze provést tak, že se vytvoří nová \verb|SSL| dle dříve definovaného 
kontextu, následně jí bude přidělen socket file descriptor, přes který po
úspěšném TLS handshake probíhá komunikace \cite{osslSSL}. Celý tento proces zastiňuje
komponenta \verb|BIO|. Konkrétní příklady užití obou způsobů jsou součástí
poslední kapitoly. Implementační detaily nejsou pro tvorbu kryptografického modulu
podstatné, rozhraní SSL API je však využito při jeho testování.